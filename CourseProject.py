def print_open_screen():
    HANGMAN_ASCII_ART = "  _    _\n | |  | |\n | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  \n |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ \n | |  | | (_| | | | | (_| | | | | | | (_| | | | |\n |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_| \n                      __/ | \n                     |___/ \n"
    MAX_TRIES = 6
    print(HANGMAN_ASCII_ART, MAX_TRIES)

def choose_word(file_path, index):
    """
    this function will choose word to guess
    :param file_path: the path of the words file 
    :param word: the number of the word in the word list
    :type: int
    :return: the length of the list and the word in the given location
    :rtype: tuple
    """
    file = open(file_path, "r")
    listed_file = str(file.read()).split(" ")
    length = len(set(listed_file))
    if len(listed_file) == 1:
        place = 0
    elif index > len(listed_file):
        place = abs(len(listed_file) - index) - 1
    else:
        place = index - 1
    my_tuple = (length, listed_file[place])

    return my_tuple

def show_hidden_word(secret_word, old_letters_guessed):
    """
    this function will show the hissen word with the letters the user guessed
    :param secret_word: the secret word needed to be guessed
    :type: string
    :param: old_letters_guessed: the letter that the user already guessed
    :type: list
    :return: the hidden word ith the letters the user guessed
    :rtype: string
    """
    new_str = ''
    for letter in secret_word:
        if letter in old_letters_guessed:
            new_str += letter + ' '
        elif letter == ' ':
            new_str += '  '
        elif not letter in old_letters_guessed:
            new_str += '_ '
            
    return new_str.strip()

def print_hangman(num_of_tries):
    """
    this function will print the correct state according to the times that the user tried to guess
    :param num_of_tries: the times that the user tries to guess
    :type: int
    :return: None
    """
    HANGMAN_PHOTOS = {'picture 1': '\nx-------x', 'picture 2': '\nx-------x\n|\n|\n|\n|\n|', 'picture 3': '\nx-------x\n|       |\n|       0\n|\n|\n|', 'picture 4': '\nx-------x\n|       |\n|       0\n|       |\n|\n|', 'picture 5': '\nx-------x\n|       |\n|       0\n|      /|\\\n|\n|', 'picture 6': '\nx-------x\n|       |\n|       0\n|      /|\\\n|      /\n|', 'picture 7': '\nx-------x\n|       |\n|       0\n|      /|\\\n|      / \\\n|'}
    tries = "picture " + str(num_of_tries + 1)
    print(HANGMAN_PHOTOS[tries])

def check_valid_input(letter_guessed, old_letters_guessed):
    """
    this function will check the validity of the letter that the player currently guessed
    :param letter_guessed: the letter that the user currrently guessed
    :type: string
    :param old_letters_guessed: the letters that the player already guessed
    :type: list
    :return: if the guess is valid
    :rtype: boolean
    """
    letter_guessed = letter_guessed.lower()
    length = len(letter_guessed)
    is_letter = letter_guessed.isalpha()
    in_old_letters = letter_guessed in old_letters_guessed
    if (length >= 2) or (is_letter == False) or (in_old_letters == True):
        valid = False
    elif (length == 1) and (is_letter == True) and (in_old_letters == False):
        valid = True
    return valid

def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """
    this function will add the letter to the guessed list or will tell that the letter is not valid
    :param letter_guessed: the letter that the user currrently guessed
    :type: string
    :param old_letters_guessed: the letters that the player already guessed
    :type: list
    :return: if the letter is valid or not
    :rtype: boolean
    """
    valid = check_valid_input(letter_guessed, old_letters_guessed)
    letter_guessed = letter_guessed.lower()
    old_letters_guessed = sorted(old_letters_guessed)
    if valid: 
        old_letters_guessed += [letter_guessed]
    else:
        new_list = ' -> '.join(old_letters_guessed)
        print('X\n' + new_list)
        
    return valid
    
def check_win(secret_word, old_letters_guessed):
    """
    thie function eill check if the user guessed all the letters in the word
    :param secret_word: the secret word needed to be guessed
    :type: string
    :param: old_letters_guessed: the letter that the user already guessed
    :type: list
    :return: if the user won or not
    :rtype: boolean
    """
    win = True
    for letter in secret_word:
        if not letter in old_letters_guessed:
            win = False
        else:
            continue
    return win 

def main():
    num_of_tries = 0
    old_letters_guessed = []
    win = False
    print_open_screen()
    word_file_path = input("Enter word file path: ")
    word_in_file = int(input("Enter number of the word you want: "))
    secret_word = choose_word(word_file_path, word_in_file)[1]
    print("Let's start!")
    print_hangman(num_of_tries)
    print(show_hidden_word(secret_word, old_letters_guessed))
    while num_of_tries < 6 and win != True:
        letter_guessed = input("Guess a letter: ")
        valid = try_update_letter_guessed(letter_guessed, old_letters_guessed)
        if valid == True:
            old_letters_guessed += [letter_guessed]
            if letter_guessed in secret_word:
                print(show_hidden_word(secret_word, old_letters_guessed))
            else: 
                print(":(")
                num_of_tries += 1
                print_hangman(num_of_tries)
                print(show_hidden_word(secret_word, old_letters_guessed))
        win = check_win(secret_word, old_letters_guessed)
    if win == True:
        print(show_hidden_word(secret_word, old_letters_guessed))
        print("WIN")
    else: 
        print(show_hidden_word(secret_word, old_letters_guessed))
        print("LOSE")
            
if __name__ == "__main__":
    main()