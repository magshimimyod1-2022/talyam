import requests
from collections import Counter

def find_most_common_words(file_path, number_of_words):
    """
    function that find the n most common words in a file
    :param file_path: the path of the file to check
    :param number_of_words: the number of common words to find
    :return: the sentence made out of the n most common words in the file
    :rtype: string
    """
    sentence = ""
    with open(file_path, "r") as opened_file:
        r_file = opened_file.read()
    listed_file = r_file.split()
    counter = Counter(listed_file)
    most_comn_words = counter.most_common(number_of_words)
    for element in most_comn_words:
        sentence += element[0] + " "
    return sentence.strip()



def extract_password_from_site():
    """
    function that go through all the files, get the 100 char out of every file and find the password
    :param: none
    return: the password
    rtype: string
    """
    password = ""
    for i in range(11, 35):
        link = "http://webisfun.cyber.org.il/nahman/files/file" + str(i) + ".nfo"
        response = requests.get(link)
        password += response.text[99]
    return password

def main():
    menu_choice = int(input("Enter your choice, 1 = password level a, 2 = sentence level b: "))
    if menu_choice == 1:
        print(extract_password_from_site())
    elif menu_choice == 2:
        path = "words.txt"
        common_words_number = 6
        print(find_most_common_words(path, common_words_number))

if __name__ == "__main__":
    main()
    