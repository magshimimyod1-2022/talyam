from ast import Continue
import socket
import datetime

def calculate_checksum(city, day):
    """
    function that calculate the checksum of the city and date
    :param city: the city to calculate
    :param day: the date to calculate
    :return: the checksum 
    """
    checksum1 = 0
    checksum2 = 0
    listed_city = city.split(",")
    listed_day = day.split("/")
    new_date = "".join(listed_day)
    for char in listed_city[0]:
        if char == " ":
            Continue
        else:
            checksum1 += ord(char.lower()) - 96 
    for num in new_date:
        checksum2 += int(num)
    total_checksum = str(checksum1) + "." + str(checksum2)

    return total_checksum

def weather_function(city ,day):
    """
    this function get the weather from the weather client server
    :param city: the city you want to check the weather in
    :param day: the date you want to check the weather on
    :return: the weather and description or number of error and error description
    """
    SERVER_IP = "34.218.16.79"
    SERVER_PORT = 77

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)

    checksum = calculate_checksum(city, day)
    city_new = (city.split(","))[0]
    msg = "100:REQUEST:city=" + city_new + "&date=" + day + "&checksum=" + checksum
    sock.sendall(msg.encode())
    server_msg = sock.recv(1024)
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    server_msg_num = server_msg[0:3]
    if server_msg_num == "200":
        start_tmp = server_msg.find("temp=") + 5
        start_txt = server_msg.find("&text=")
        temp = server_msg[start_tmp:start_txt]
        text = server_msg[start_txt + 6:]
        weather_tuple = (temp, text)
    else:
        error = 999
        start_error_txt = server_msg.find("ERROR:") + 6
        error_txt = server_msg[ start_error_txt:]
        weather_tuple = (error, error_txt)
    
    return weather_tuple
        

def main():
    city = input("Enter the place you live in (city,country): ")
    menu_choice = int(input("Enter 1 to see today's weather or 2 to see today's weather and in the next 3 days: "))
    today_date = datetime.date.today()
    today_date = today_date.strftime("%d/%m/%Y")
    if menu_choice == 1:
        weather = weather_function(city ,today_date)
        if weather[0] == 999:
            print(str(weather[0]) + ", ERROR: " + weather[1])
        else:
            print(today_date + ", Temperature: " + str(weather[0]) + ", " + weather[1])
    elif menu_choice == 2:
        for i in range(4):
            date =  (datetime.datetime.today() + datetime.timedelta(days=i)).strftime("%d/%m/%Y")
            weather = weather_function(city ,date)
            if weather[0] == 999:
                print(str(weather[0]) + ", ERROR: " + weather[1])
                break
            else:
                print(date + ", Temperature: " + str(weather[0]) + ", " + weather[1])
                

if __name__ == "__main__":
    main()